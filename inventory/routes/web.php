<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/index', [
    'uses' => 'CrudController@index',
    'as' => 'index'
]);
Route::get('/create', [
    'uses' => 'CrudController@create',
    'as' => 'create'
]);
Route::get('/show', [
    'uses' => 'CrudController@show',
    'as' => 'show'
]);
Route::get('/edit', [
    'uses' => 'CrudController@edit',
    'as' => 'edit'
]);
Route::get('/delete', [
    'uses' => 'CrudController@delete',
    'as' => 'delete'
]);