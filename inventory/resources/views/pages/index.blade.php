@extends('layouts.Master')

@section('title')
    CRUD
    @endsection
    @section('contect')




<div class = 'container'>
    <h3>Test CRUD</h3>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <a class="btn btn-success" href="{{ route('create') }}"> Create New Product</a>
            </div>
        </div>
    </div>



    <table>
        <thead>
        <th>id</th>
        <th>product</th>


        </thead>
        <tbody>
       <!-- foreach($createorders as $Createorder)-->

            <tr>
                <td></td>
                <td></td>

                <td>
                    <div class = 'row'>
                        <a href = '{{route('show')}}' class = 'viewShow btn-floating orange' data-link = '#'><i class = 'material-icons'>info</i></a>
                        <a href = '{{route('edit')}}' class = 'viewEdit btn-floating blue' data-link = '#'><i class = 'material-icons'>edit</i></a>
                        <a href = '{{route('delete')}}' class = 'delete btn-floating modal-trigger red' data-link = "#" ><i class = 'material-icons'>delete</i></a>

                    </div>
                </td>
            </tr>
        <!--endforeach-->
        </tbody>
    </table>
</div>
<div id="modal1" class="modal">
    <div class = "row AjaxisModal">
    </div>
</div>


<script> var baseURL = "{{URL::to('/')}}"</script>
<script src = "{{ URL::asset('js/AjaxisMaterialize.js')}}"></script>
<script src = "{{ URL::asset('js/scaffold-interface-js/customA.js')}}"></script>
